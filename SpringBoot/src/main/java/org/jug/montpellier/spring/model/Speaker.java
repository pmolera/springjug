package org.jug.montpellier.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;

//@Entity
public class Speaker extends AbstractModel {
	@Column(name="FULLNAME"  , length=150 , nullable=true , unique=true)
	public String fullName;
	@Column(name="ACTIVITY"  , length=150 , nullable=true , unique=true)
	public String activity;
	@Column(name="COMPANY"  , length=150 , nullable=true , unique=true)
	public String company;
	@Column(name="URL"  , length=150 , nullable=true , unique=true)
	public String url;
	@Column(name="PERSONALURL"  , length=150 , nullable=true , unique=true)
	public String personalUrl;
	@Column(name="EMAIL"  , length=150 , nullable=true , unique=true)
	public String email;
	@Column(name="DESCRIPTION"  , length=5000 , nullable=true , unique=true)
	public String description;
	@Column(name="JUGMEMBER"  , length=150 , nullable=true , unique=true)
	public Boolean jugmember;
	@Column(name="MEMBERFCT"  , length=150 , nullable=true , unique=true)
	public String memberFct;
	@Column(name="PHOTOURL"  , length=150 , nullable=true , unique=true)
	public String photoUrl;
}
