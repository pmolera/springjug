package org.jug.montpellier.spring.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jug.montpellier.spring.model.Event;
import org.jug.montpellier.spring.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

 @Service (value="eventService")
 @Transactional
 public class EventServiceImpl implements EventService {

    UserService userService;

     public void setUserService(UserService userService) {
         this.userService = userService;
     }

     @Override
     public void eventMethod() {
         List<User> users =  userService.findAllUsers();
         // code metier ici
     }



     @Override
	public Collection<Event> getAllEvents() {
		List<Event> events = new ArrayList<Event>();
		Event event = new Event();
		event.setId(new Long(1));
		event.setTitle("test");
		events.add(event);
		return events;
	}




     public UserServiceImpl getUserService() {
         return (UserServiceImpl)userService;
     }
 }


