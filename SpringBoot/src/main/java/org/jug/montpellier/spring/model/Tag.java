package org.jug.montpellier.spring.model;

import javax.persistence.Entity;

@Entity
public class Tag extends AbstractModel{

    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
