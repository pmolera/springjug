package org.jug.montpellier.spring.service;

import javax.annotation.Resource;

import org.jug.montpellier.spring.model.User;
import org.jug.montpellier.spring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

 @Service("userService")
 public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;


    @Override
    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void addUser(User user) {
        userRepository.save(user);
    }

    @Override
    public User getUser(String firstName, String lastName) {
       return userRepository.findByFirstnameAndLastname(firstName, lastName);
    }

     @Override
     public User getUser(String id) {
         return userRepository.findOne(new Long(id));
     }
}
