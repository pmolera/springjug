package org.jug.montpellier.spring.model;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Talk extends AbstractModel {

    public int orderInEvent;

    public String title;

    @Lob@Column(length=5)
    public String time;

    @ManyToOne
    public Speaker speaker;

    @ManyToMany(cascade= CascadeType.PERSIST)
    public Set<Tag> tags;

    @Lob@Column(length=2000)
    public String teaser;

    @ManyToOne
    public Event event;

    public int getOrderInEvent() {
        return orderInEvent;
    }

    public void setOrderInEvent(int orderInEvent) {
        this.orderInEvent = orderInEvent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Speaker getSpeaker() {
        return speaker;
    }

    public void setSpeaker(Speaker speaker) {
        this.speaker = speaker;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public String getTeaser() {
        return teaser;
    }

    public void setTeaser(String teaser) {
        this.teaser = teaser;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public String toString() {
        return "Talk{" +
                "title='" + title + '\'' +
                ", speaker=" + speaker.toString() +
                ", teaser='" + teaser + '\'' +
                ", tags=" + tags +
                '}';
    }
}
