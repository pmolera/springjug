package org.jug.montpellier.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;

//@Entity
public class EventPartner extends AbstractModel{
	/**
	* Unique name
	*/
	public String name;
	/**
	* Partner URL
	*/
	public String url;
	/**
	* Partner description. Can be wikitext formated.
	*/
	//@Column(length=10000)
	public String description;
	/**
	* Local or remote logo URL
	*/
	public String logoURL;

}
