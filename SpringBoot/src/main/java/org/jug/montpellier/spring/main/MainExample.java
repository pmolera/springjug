package org.jug.montpellier.spring.main;

import org.jug.montpellier.spring.model.User;
import org.jug.montpellier.spring.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created by pigalon on 11/10/2014.
 */
public class MainExample {

    public void main(String args[]) {

        ApplicationContext ctx =
                new ClassPathXmlApplicationContext("examples/application-context.xml");

        UserService userService = (UserService) ctx.getBean("userService");

        List<User> users = userService.findAllUsers();
    }
}
