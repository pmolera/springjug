package org.jug.montpellier.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class User extends AbstractModel {

    @Column(name="EMAIL")
    public String email;

    @Column(name="FIRSTNAME")
    public String firstName;

    @Column(name="LASTNAME")
    public String lastName;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
