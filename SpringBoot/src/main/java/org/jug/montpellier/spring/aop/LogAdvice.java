package org.jug.montpellier.spring.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
 @Component("logAdvice")
 public class LogAdvice {

    @Pointcut("execution(* org.jug.montpellier.service.*.*(..))")
    public void anyService(){}


    @Around("anyService()")
    public Object logAction(ProceedingJoinPoint pjp)throws Throwable{
        doLog(pjp, " ======>");
        Object obj = pjp.proceed();
        doLog(pjp, " <======");
        return obj;
    }
    /**
     * @param point
     */
    private void doLog(JoinPoint point, String finMess) {
        Class<? extends Object> pointClass = point.getTarget().getClass();
        Logger logger = LoggerFactory.getLogger(pointClass);
        logger.debug(point.getSignature().getName() + finMess);
// TODO simplify class Name ??
    }
}