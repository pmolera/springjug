package org.jug.montpellier.spring.controller;

import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jug.montpellier.spring.model.Event;
import org.jug.montpellier.spring.model.User;
import org.jug.montpellier.spring.service.EventService;
import org.jug.montpellier.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
 public class HomePageController {

    @Autowired
    UserService userservice;

    @RequestMapping("/home")
    protected ModelAndView home() throws Exception{
        List<User> users = userservice.findAllUsers();
        return new ModelAndView("home", "users", users);
    }

    @RequestMapping("/user")
    protected void list(@RequestParam("id") String id, Model model) throws Exception{
        User user = userservice.getUser(id);
        model.addAttribute("user", user);
    }

}
