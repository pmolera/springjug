package org.jug.montpellier.spring.service;

import java.util.Collection;

import org.jug.montpellier.spring.model.Event;

public interface EventService {

    void eventMethod();

	Collection<Event> getAllEvents();
}
