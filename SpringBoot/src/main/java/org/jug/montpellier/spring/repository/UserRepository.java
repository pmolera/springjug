package org.jug.montpellier.spring.repository;

import org.jug.montpellier.spring.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository  extends JpaRepository<User, Long> {
    User findByFirstnameAndLastname(String firstName, String lastName);
}
