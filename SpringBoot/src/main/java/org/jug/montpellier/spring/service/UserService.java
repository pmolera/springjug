package org.jug.montpellier.spring.service;


import org.jug.montpellier.spring.model.User;

import java.util.List;

public interface UserService {

	List<User> findAllUsers();

    void addUser(User user);

    User getUser(String firstName, String lastName);

    User getUser(String id);
}
