package org.jug.montpellier.spring.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

//@Entity
public class News extends AbstractModel{
	//@Column(name="TITLE")
	public String title;
	/**
	* The Date the news is posted
	*/
	public Date date;
	
	//@Column(name="CONTENT", length=10000)
	public String content;
	/**
	* Display/Activate or not Disqus.com comments for this News
	*/
	//@Column(name="COMMENTS")
	public boolean comments;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isComments() {
        return comments;
    }

    public void setComments(boolean comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "News{" +
                "title='" + title + '\'' +
                ", date=" + date +
                ", content='" + content + '\'' +
                ", comments=" + comments +
                '}';
    }
}
