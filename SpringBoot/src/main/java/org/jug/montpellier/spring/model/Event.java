package org.jug.montpellier.spring.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Event extends AbstractModel{
    public String title;
    public Date date;
	int capacity;
	String description;
	String location;
	Boolean registrationUrl;
    public boolean open;

    @Lob @Basic(fetch= FetchType.LAZY)
    @Column(name="REPORT")
	String report;

    @Lob @Basic(fetch= FetchType.LAZY)
    @Column(name="MAP", length = 5000)
    String map;

	Long partnerId;

    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL)
    public List<Talk> talks;
	

	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getMap() {
		return map;
	}
	public void setMap(String map) {
		this.map = map;
	}
	public Boolean getRegistrationUrl() {
		return registrationUrl;
	}
	public void setRegistrationUrl(Boolean registrationUrl) {
		this.registrationUrl = registrationUrl;
	}
	public String getReport() {
		return report;
	}
	public void setReport(String report) {
		this.report = report;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Long getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(Long partnerId) {
		this.partnerId = partnerId;
	}

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public List<Talk> getTalks() {
        return talks;
    }

    public void setTalks(List<Talk> talks) {
        this.talks = talks;
    }

    @Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
        sb .append("Event [ capacity=" + capacity + ", description="
            + description + ", location=" + location + ", map=" + map
            + ", registrationUrl=" + registrationUrl + ", report=" + report
            + ", title=" + title + ", partnerId=" + partnerId
            + ", date=" + date + ", open=" + open + "\n");
        sb.append("      talks : \n");
            for(Talk talk : talks) {
                sb.append("       " + talk.toString() + "\n");
            }
        sb.append("]");

        return sb.toString();
	}
}
